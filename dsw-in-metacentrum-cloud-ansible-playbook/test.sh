#!/bin/bash

ansible -i hosts dswservers  -u debian -b -m ping
ansible -i hosts dswservers  -u debian -b -m shell -a '/usr/bin/id; /usr/bin/hostname -f; /usr/bin/ip addr'
ansible -i hosts dswservers  -u debian -b -m setup
ansible-playbook -i hosts test.yml
