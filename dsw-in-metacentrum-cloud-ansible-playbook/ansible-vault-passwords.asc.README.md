# MetaCentrum Cloud DSW Ansible Config

## Ukázkové Ansible Vault heslo

Dešifrování ukázkového Ansible Vault hesla:
```bash=
$ gpg --batch --passphrase 'dsw_ukazkove_heslo' ansible-vault-passwords.asc
```

Soubor `ansible-vault-passwords.asc` je tedy symetricky šifrován utilitou GPG 
heslem `dsw_ukazkove_heslo`.
