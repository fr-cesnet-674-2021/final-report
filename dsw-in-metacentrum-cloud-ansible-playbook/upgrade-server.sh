#!/bin/bash

ansible -vv -i hosts dswservers -u debian -b -m apt -a 'update_cache=yes upgrade=yes' -e 'NEEDRESTART_MODE=automatically'
